'use strict';

import Phaser from 'phaser';
import DragonSmall from '../components/dragon-small';

export default class Game extends Phaser.State {
    preload() {

    }

    create() {


        // create bg
        this.game.add.sprite(0, 0, 'SafeZone');

        this.initLevel();
    }

    update() {

    }

    initLevel() {
        this.dragons = this.createDragons();


    }

    clearLevel() {

    }

    createDragons() {
        let dragonSmall = new DragonSmall(this.game, 90, 65);
        this.game.add.group(0, 0, dragonSmall);

    }
}
