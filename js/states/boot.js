'use strict';

import Phaser from 'phaser';

export default class Boot extends Phaser.State {
    create() {
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.onSizeChange.add(this.updateBg, this);

        document.ontouchmove = function(event){
            event.preventDefault();
        };

        this.game.scale.pageAlignHorizontally = true;

        this.game.state.start('Preload');
    }

    preload() {
        this.game.load.image('loading', 'assets/loading.png');
    }

    updateBg() {
        let scale = this.game.scale.width / 960;

        let el = document.getElementById("main");
        el.style.background = 'url(assets/Bg_1920x1280.png) no-repeat center center';
        el.style.backgroundSize=1920 * scale + 'px ' + 1280 * scale + 'px';
        console.log(1920 * scale + 'px ' + 1280 * scale + 'px');
    }

}
