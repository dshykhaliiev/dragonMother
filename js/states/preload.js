import Phaser from 'phaser'

export default class Preload extends Phaser.State {
    preload() {
        console.log("PRELOAD")
        this.loading = this.game.add.sprite(0, 0, 'loading');
    }

    create() {
        this.game.load.image('SafeZone', 'assets/SafeZone.png');
        this.game.load.image('dragon_small', 'assets/dragon_small.png');
        this.game.load.image('dragon_medium', 'assets/dragon_medium.png');
        this.game.load.image('dragon_big', 'assets/dragon_big.png');

        this.game.load.onLoadComplete.add(this.onLoadComplete, this);
        this.game.load.start();
    }

    update() {
        console.log("update");
        // this.loading.x += 1;
    }

    onLoadComplete() {
        this.game.state.start('Game');
        console.log("COMPLETE");
    }
}
