'use strict';

import 'pixi';
import 'p2';
import Phaser from 'phaser';
import Boot from './states/boot';
import Preload from './states/preload';
import Game from './states/game';

var game = new Phaser.Game(960, 640, Phaser.AUTO);

game.state.add('Preload', Preload);
game.state.add('Boot', Boot);
game.state.add('Game', Game);


game.state.start('Boot');
