import Phaser from 'phaser';

class DragonBase extends Phaser.Group {
    constructor(game, id) {
        super(game);

        this.game = game;
        this.id = id;

        this.init();
    }

    init() {
        this.createGraphics();

        // this.swipe = this.game.input.mousePointer;

        this.game.input.onDown.add(this.onTouchDown, this);
        this.game.input.onUp.add(this.onTouchRelease, this);

        // this.game.input.addMoveCallback(this.drag, this);

    }

    createGraphics() {
        let sprite = this.create(0, 0, this.id);
        sprite.anchor.setTo(0.5, 0.5);
    }

    update() {
        // console.log(">>>: ", this.game.input.activePointer.position);
    }

    onTouchDown() {
        // console.log("TOUCH DOWN: ", this.touched, this.game.input.activePointer.position );
        let pointerPos = this.game.input.activePointer.position;
        // console.log(">>: ", this.x, this.y, pointerPos, Phaser.Point.distance(this, pointerPos))

        if (Phaser.Point.distance(this, pointerPos) <= 180)
        {
            this.touchStartPos = new Phaser.Point();
            this.touchStartPos = pointerPos.copyTo(this.touchStartPos);
            this.touchStartTime = this.game.time.now;
            this.touched = true;
        }
    }

    onTouchRelease() {
        // console.log("TOUCH RELEASE: ", this.touched, this.game.input.activePointer.position );
        if (this.touched) {

            let pointerPos = this.game.input.activePointer.position;
            // console.log("!!!!: ", pointerPos, this.touchStartPos, Phaser.Point.distance(pointerPos, this.touchStartPos))

            this.touched = false;

            if (Phaser.Point.distance(pointerPos, this.touchStartPos) >= 50 && this.game.time.now - this.touchStartTime < 500) {
                this.attack();
            }
        }

    }

    attack() {
        // overrided in subclasses
    }

    returnBack() {
        // overrided in subclasses
    }
}

export default DragonBase;