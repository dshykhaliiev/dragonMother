import Phaser from 'phaser';
import DragonBase from '../components/dragon-base';

class DragonSmall extends  DragonBase {
    constructor(game, x, y) {
        super(game, 'dragon_small');

        this.game = game;
        this.x = x;
        this.y = y;
        this.startX = x;
        this.startY = y;
    }

    attack() {
        let attackTween = this.game.add.tween(this).to( { x: (960 / 2), y: 640 - 80}, 500, Phaser.Easing.Linear.None, true);
        attackTween.onComplete.addOnce(this.returnBack, this);
        // this.returnTween = this.game.add.tween(this).to( { x: (960 / 2), y: 640 - 80}, 500, Phaser.Easing.Linear.None, true);

        // this.attackTween.chain(this.returnTween)
    }

    returnBack() {
        this.game.add.tween(this).to( { x: this.startX, y: this.startY}, 500, Phaser.Easing.Linear.None, true);
    }
}

export default DragonSmall;